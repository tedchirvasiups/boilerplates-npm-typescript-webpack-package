import {ClassA, ClassB} from 'mypackage'

var a: ClassA = new ClassA();
//Hello from ClassA!
a.hello();

var b: ClassB = new ClassB();
//Hello from ClassB!
b.hello();