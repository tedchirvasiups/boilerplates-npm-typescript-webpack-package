# Detailed tutorial:  
1. **NPM configuration**  
    1. Open command prompt in the project folder (you can use the "cd" command to navigate to the project directory)  
    2. Run "npm init -y" to generate the "package.json" file with default configuration.  
    3. Open the "package.json" file and edit the package details (package name, description, author, version)  
    4. Run "npm install typescript webpack webpack-cli ts-loader clean-webpack-plugin --save-dev" to install the typescript compiler, webpack and the typescript loader for webpack.  

2. **Typescript configuration**  
    1. Run "npx typescript --init" to generate the "tsconfig.json" file and set in "compilerOptions" (check the examples section)  
        * "moduleResolution": "node"  
        * "baseUrl": "./"  
        * "outDir": ""  
        * "declaration": true  
    2. In the root directory create a "src" folder. This will contain the source Typescript files  
    3. In the "src" folder add an "index.ts" file. This file will export all the modules of the package (check the examples section)  

3. **Webpack configuration**
    1. Create a "dist" folder in the root directory. This is where your bundles will be created  
    2. Create the "webpack.config.js" with the standard setup for [Typescript](https://webpack.js.org/guides/typescript/). Make sure that in "output" you specify "libraryTarget": "umd" and the public path is "." (check the examples section)  
    You run "npx webpack init" to have it scaffold the basic configuration file for you
    3. In the "package.json" file add to the scripts section:
        * "start": "set NODE_ENV=development&& webpack -d"  
        * "watch": "set NODE_ENV=development&& webpack -d --watch"  
        * "build": "set NODE_ENV=production&& webpack -p"  
    Setting the NODE_ENV makes sure the environment variable is set to the correct build mode. If you don't use process.env.NODE_ENV, you can skip adding that.  
    4. In the "package.json" file add:  
        * "main": "dist/mypackage.js"  
        * "types": "dist/index.d.ts"  
    5. Run "npm run build" and voilla! In the dist folder you should have the bundle and the typings.  
    You can run "npm run watch" to run the build task in watch mode and update the bundle everytime you make any changes, or "npm run start" to build it once in development mode. 

4. **Development**
**TODO**

5. **Bitbucket hosting**
**TODO**