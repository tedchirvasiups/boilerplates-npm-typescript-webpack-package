# Scope
The goal is to learn to write reusable Typescript packages, installable through [npm](https://www.npmjs.com) without the need to publish them publicly and without the need to use private NPM packages.  
The package would simply be installed from a (remote) git repository and then imported in Typescript without manually specifying aliases.  
This works for plain old Javascript packages too.  

Installation:
```shell
npm install mypackage-repo-address#version
```
Usage:
```typescript
import * as package from 'mypackage'
```
**When "moduleResolution" is set to "node" in the "tsconfig.json" file, Typescript and Webpack will automatically resolve packages inside the node_modules folder.**  

# Tools required:  
* [NPM](https://www.npmjs.com)  
* [Node.js](https://nodejs.org/en/)  
* [Git](https://git-scm.com) (if hosting on [BitBucket](https://bitbucket.org))


# Short tutorial:  
### Packaging
1. Run "npm init -y"  
2. Run "npm install typescript webpack webpack-cli ts-loader clean-webpack-plugin --save-dev"  
3. Create the "tsconfig.json" file and set in "compilerOptions" (check the examples section)  
    * "moduleResolution": "node"  
    * "baseUrl": "./"  
    * "outDir": ""  
    * "declaration": true  
4. Create a "src" folder and inside add an "index.ts" file. This file will export all the modules of the package (check the examples section)  
5. Create a "dist" folder in the root directory. Exclude it from tsconfig.json.  
6. Create the "webpack.config.js" with the standard setup for [Typescript](https://webpack.js.org/guides/typescript/). Make sure that in "output" you specify "libraryTarget": "umd" and the public path is "." (check the examples section)  
7. In the "package.json" file add to the scripts section:  
    * "build": "webpack -p"  
8. In the "package.json" file add:  
    * "main": "dist/mypackage.js"  
    * "types": "dist/index.d.ts"  
9. Run "npm run build" and in the dist folder you should have the bundle and the typings.  

### Development
* For development you could create a "dev" folder with its own "webpack.config.js" and "tsconfig.json" files and reference the bundle from the dist folder in the root. Just make sure you **exclude** this dev folder from the main build in "tsconfig.json". Check out the dev folder in this repository.  
* I also installed [html-webpack-plugin](https://github.com/jantimon/html-webpack-plugin) (npm install html-webpack-plugin --save-dev) for development so it automatically replaces the reference to the old bundle with the new one.  
* Since this is sort of a fake "consumer" because it's not installed in node_modules, you have to alias it in both the "tsconfig.json" file and "webpack.config.js" file. Make sure tsconfig.json references the typings file (index.d.ts in this case) and webpack.config.js references the actual javascript bundle (mypackage.js in this case). You could name your typings file and the bundle the same if you wish.  
* Run "npx webpack -p" from the dev folder.  
* Open the index.html from the dev/dist folder in your favourite browser and check the console.

**Note: if using VSCode, you might need to hit Ctrl+Shfit+P and hit "Reload Window" to get the compiler to recognize the new typings file and aliases! (or simply restart VSCode)**

### Bitbucket hosting


# Extended tutorial
For a bit more detailed version of this tutorial, check out the README.extended.md file (not completed).

# Examples:  
tsconfig.json:  
```json
{
  "compilerOptions": {
    "target": "es3",
    "moduleResolution": "node",
    "baseUrl": "./",
    "outDir": "",
    "declaration": true
  },
  "exclude": [
    "dev/*",
    "dist/*"
  ]
}
```

index.ts:
```typescript
export * from './ClassA'
export * from './ClassB'
```

webpack.config.js:
```javascript
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: {
        'mypackage': './src/index.ts',
    },
    resolve: {
        extensions: ['.js', '.ts'],
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                include: [path.resolve(__dirname, "src")]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin([path.resolve(__dirname, 'dist')])
    ],
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '.',
        library: "mypackage",
        libraryTarget: "umd"
    }
};
```